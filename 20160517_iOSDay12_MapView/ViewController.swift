//
//  ViewController.swift
//  20160517_iOSDay12_MapView
//
//  Created by ChenSean on 5/17/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit
import MapKit

class MyMKPointAnnotation: MKPointAnnotation {
    var id: String!
}

class ViewController: UIViewController, MKMapViewDelegate {
    @IBAction func myViewButtonClick(sender: AnyObject) {
        print(selectAnnId)
    }
    
    @IBOutlet weak var myViewLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var myView: UIView!
    
    var selectAnnId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // 產生大頭針
        let ann = MyMKPointAnnotation()
        // 帶經緯度座標
        ann.coordinate = CLLocationCoordinate2DMake(24.402551, 121.161865)
        ann.title = NSLocalizedString("Title_雪霸國家公園", comment: "") // comment 只是一個備註幫助開發者知道這是啥
        ann.subtitle = NSLocalizedString("Title_台灣", comment: "")
        ann.id = "SnowBar"
        
        let region = MKCoordinateRegionMakeWithDistance(ann.coordinate, 10000, 0)
        
        let ann2 = MyMKPointAnnotation()
        ann2.coordinate = CLLocationCoordinate2DMake(24.148666, 120.664225)
        ann2.title = "勤美綠園道"
        ann2.subtitle = "台中"
        ann2.id = "ChingMay"
        
        mapView.delegate = self
        
        mapView.addAnnotation(ann) //將大頭針插到地圖上面
        mapView.addAnnotation(ann2)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        // 大頭針的 callback function
        // 使用 call back function 都要習慣寫這行
        if annotation is MKUserLocation {
            // 藍點的顏色是無法客製，如果改到 MKUserLocation 就會 runtime crash. // 會被退件喔！
            return nil
        }
        
        // 從 pin 這個 memory pool 找有沒有可以二手回收的大頭針
        var annView = mapView.dequeueReusableAnnotationViewWithIdentifier("pin") as? MKPinAnnotationView // 轉型為 MKPinAnnotationView 才有顏色可以用
        if annView == nil {
            annView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin") // 如果沒有二手的，就 new 一個新的，並且加到 pin 這個 memory pool 裡面
        }
        
        annView?.canShowCallout = true;
        
        return annView
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        // 參數的這個 view 就是點到的大頭針
        
        let annView = view as? MKPinAnnotationView
        annView?.pinTintColor = UIColor.yellowColor()
        annView?.leftCalloutAccessoryView = UIImageView(image: UIImage(named: "Icon-Small-50@2x"))
        annView?.detailCalloutAccessoryView = myView
        
        let widthConstraint = myView.widthAnchor.constraintEqualToConstant(240)
        let heightConstraint = myView.heightAnchor.constraintEqualToConstant(128)
        
        annView?.detailCalloutAccessoryView?.addConstraint(widthConstraint)
        annView?.detailCalloutAccessoryView?.addConstraint(heightConstraint)

        let ann = annView?.annotation as! MyMKPointAnnotation
        selectAnnId = ann.id;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

